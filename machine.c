/**
   This program deals with SPIM instructions. The 
   instructions are in the form of:
   0x"Opcode""R1""R2""R3""Address"
   Opcode: 4 bytes, Ranges from 0-13 with each having a unique
   set of actions, and having different register inputs and
   memeory requirements
   R1: 4 Bytes, Ranges from 0-11
   R2: 4 Bytes, Ranges from 0-11
   R3: 4 Bytes, Ranges from 0-11
   Address: 16 Bytes, Ranges from 0-49152 
   Author: Abdelrahman Hammad    Student ID: 112394561
   Section: 0104                 TA: Mathew
   Directory ID: ahammad
 **/
#include <stdio.h>
#include "machine.h"




/**
   Variable declaration to facilitate functions
   1. Counter: Counter variable for loops
   2. separated_values:An array with a length of 5 that stores the
   separate sections of the SPIM instructions
   3. uses_register1: An array that stores whether or not an 
   opcode uses register 1.
   4. uses_register2: An array that stores whether or not an 
   opcode uses a register 2.
   5. uses_register3: An array that stores whether or not an 
   opcode uses a register 3
   6. uses_memory: An array that stores whether or not an 
   opcode uses a memory.
 **/

int counter = 0;
unsigned int separated_values[5] = {0};
int uses_register1 [14] = {0,1,1,1,1,1,1,1,1,1,1,1,1,1};
int uses_register2 [14] = {0,1,1,1,1,1,1,0,0,0,1,1,0,0};
int uses_register3 [14] = {0,1,1,0,0,1,0,0,0,0,0,0,0,0};
int uses_memory [14] = {0,0,0,0,1,0,0,1,1,1,0,1,0,0};




void print_instruction(Word instruction) {
  separate(instruction);
  /**
     Prints the opcode based on the opcode 
  **/
  if ((separated_values[0] == 0 && printf("halt")) ||
     (separated_values[0] == 1 && printf("add")) ||
     (separated_values[0] == 2 && printf("mul")) ||
     (separated_values[0] == 3 && printf("neg")) ||
     (separated_values[0] == 4 && printf("shl")) ||
     (separated_values[0] == 5 && printf("and")) ||
     (separated_values[0] == 6 && printf("not")) || 
     (separated_values[0] == 7 && printf("lw")) ||
     (separated_values[0] == 8 && printf("li")) ||
     (separated_values[0] == 9 && printf("sw")) ||
     (separated_values[0] == 10 && printf("move")) ||
     (separated_values[0] == 11 && printf("bne")) ||
     (separated_values[0] == 12 && printf("read")) ||
      (separated_values[0] == 13 && printf("write"))) {
  }
  /**
     Prints the first registry if applicable
  **/
  if (uses_register1[separated_values[0]])
    printf(" R%02d",separated_values[1]);
  /**
     Prints the second registry if applicable
   **/
  if (uses_register2[separated_values[0]])
    printf(" R%02d",separated_values[2]);
  /**
     Prints the third registry if applicable
   **/
  if (uses_register3[separated_values[0]])
    printf(" R%02d",separated_values[3]);
  /**
     Prints the memory location if applicable
   **/
  if (uses_memory[separated_values[0]])
    printf(" %05d", separated_values[4]);
  /**
     Prints a new line at the end.
   **/
  printf("\n");
}

/**
   Helper function that separates the instruction into a 
   the array separated_values
 **/
void separate (Word instruction) {
  /**
     Extracting the first element of the hex
   **/
  separated_values[0] = instruction &
    (0xF0000000);
  separated_values[0] = (separated_values[0] >> 28);
   /**
     Extracting the second element of the hex
   **/
  separated_values[1] = instruction &
    (0x0F000000);
  separated_values[1] = (separated_values[1] >> 24);
   /**
     Extracting the third element of the hex
   **/
  separated_values[2] = instruction &
    (0x00F00000);
  separated_values[2] = (separated_values[2] >> 20);
   /**
     Extracting the fourth element of the hex
   **/
  separated_values[3] = instruction &
    (0x000F0000);
  separated_values[3] = (separated_values[3] >> 16);
  /**
     Extracting the fifth element of the hex
   **/
  separated_values[4] = instruction &
    (0x0000FFFF);
}

/**
   Function that takes an array of instructions and 
   prints them if starting address and validity is 
   violated.
 **/
int disassemble(const Word memory[], int starting_address
		, int num_words){
  int element_count = 0;
  int address = starting_address;
  int index = (starting_address/4);
  /**
     Checks if memory is null and whether starting address starts at 4
   **/
  if (memory == NULL || starting_address % 4 != 0 || starting_address < 0
      || starting_address > 12287 || starting_address + num_words > 12287)
    return -1;
  /**
     If number is less than or equal to zero then
     return 0;
   **/
  if (num_words <= 0 )
    return 0;
  for (counter = index; counter < index + num_words; counter++) {
    /**
       If counter goes outside the array then end and
       return number of elements printed.
     **/
    if (counter > 12287) {
      counter = index + num_words;
    } else {
	printf("0x%04x: ", address);
	address = address + 4;
	print_instruction(memory[counter]);
	element_count++;
    }
  }
  return element_count;
  
}
/**
   Function that will take sepearate parts of a SPIM
   instruction and generate a SPIM instruction if it is valid
 **/
int encode_instruction(unsigned int opcode, unsigned int reg1,
                       unsigned int reg2, unsigned int reg3,
                       unsigned int memory_addr, Word *const instruction) {
  Word checker;
  if (instruction != NULL) {
    /**
       Shifts the bits according to their position in the 
       hexadecimal. DOes not shift memory cause it 
       is already in the correct position.
     **/
    opcode = opcode << 28;
    reg1 = reg1 << 24;
    reg2 = reg2 << 20;
    reg3 = reg3 << 16;
    /**
       Appends all the numbers together
     **/
    checker = ((((opcode|reg1)|reg2)|reg3)|memory_addr);
    if (valid_instruction(checker)) {
      *instruction = checker;
      return 1;
    } else {
      return 0;
    }
  } else {
    return 0;
  }
}

/**
Function that checks the validity of an instruction based on 
certian limitations, these limitations are:
 1. Invalid Opcode 
 2. Invalid Registry numbers if used
 3. Invalid address if used
 **/
int valid_instruction(Word instruction) {
  /**
     Variables thats stores the separate parts of the instrcuction
   **/
  int op, reg_1, reg_2, reg_3, address;
  /**
     Separates the instruction into array
   **/
  separate (instruction);
  /**
     Assign each variable to the corresponding value in array
   **/
  op = separated_values[0];
  reg_1 = separated_values[1];
  reg_2 = separated_values[2];
  reg_3 = separated_values[3];
  address = separated_values[4];
  /**
     If opcode is less than 0 or greater than 13 function returns 0
   **/
  if (op < 0 || op > 13) 
    return 0;
  /**
     If first registry is used and is less than 0 or greater
     than 11 then return 0
   **/
  if (uses_register1[op] && (reg_1 < 0 || reg_1 > 11) && op != 9
      && op != 11 && op != 13)
    return 0;
  /**
     If second registry is used and is less than 0 or greater
     than 11 then return 0 
   **/
  if (uses_register2[op] && (reg_2 < 0 || reg_2 > 11))
    return 0;
  /**
     If third registry is used and is less than 0 or greater
     than 11 then return 0
   **/
  if (uses_register3[op] && (reg_3 < 0 || reg_3 > 11))
    return 0;
  /**
     If memory address is used and is greater than 49151 bytes
   **/
  if (uses_memory[op] == 1 && address > 49151 && op != 8 && op!= 4)
    return 0;
  /**
     If memory address  is used and is not divisible by 4
   **/
  if (uses_memory[op] == 1 && address % 4 != 0 && op != 8 && op != 4)
    return 0;
  /**
     If the first registry is used and it is 0 or 11
   **/
  if(uses_register1[op] == 1 && (reg_1 == 11 || reg_1 == 0))
    return 0;
  /**
     If the shl opcode is called and a bit shift by greater than 31
     is attempted
   **/
  if(op == 4 && (address & 0x0000FFFF) > 31)
    return 0;
  /**
     Instruction is valid return 1
   **/
  return 1;
  
}


