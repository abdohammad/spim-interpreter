CFLAGS =  -ansi -pedantic-errors -Wall -Werror -Wshadow -Wwrite-strings
CC = gcc
OBJFILES = machine.o interpreter.o

all: public01.x public02.x public03.x public04.x public05.x public06.x public07.x public08.x public09.x public10.x
public01.x: public01.o $(OBJFILES)
	$(CC) public01.o $(OBJFILES) -o public01.x
public02.x: public02.o $(OBJFILES)
	$(CC) public02.o $(OBJFILES) -o public02.x
public03.x: public03.o $(OBJFILES)
	$(CC) public03.o $(OBJFILES) -o public03.x
public04.x: public04.o $(OBJFILES)
	$(CC) public04.o $(OBJFILES) -o public04.x
public05.x: public05.o $(OBJFILES)
	$(CC) public05.o $(OBJFILES) -o public05.x
public06.x: public06.o $(OBJFILES)
	$(CC) public06.o $(OBJFILES) -o public06.x
public07.x: public07.o $(OBJFILES)
	$(CC) public07.o $(OBJFILES) -o public07.x
public08.x: public08.o $(OBJFILES)
	$(CC) public08.o $(OBJFILES) -o public08.x
public09.x: public09.o $(OBJFILES)
	$(CC) public09.o $(OBJFILES) -o public09.x
public10.x: public10.o $(OBJFILES)
	$(CC) public10.o $(OBJFILES) -o public10.x
public01.o: public01.c
	$(CC) -c $(CFLAGS) public01.c
public02.o: public02.c
	$(CC) -c $(CFLAGS) public02.c
public03.o: public03.c
	$(CC) -c $(CFLAGS) public03.c
public04.o: public04.c
	$(CC) -c $(CFLAGS) public04.c
public05.o: public05.c
	$(CC) -c $(CFLAGS) public05.c
public06.o: public06.c
	$(CC) -c $(CFLAGS) public06.c
public07.o: public07.c
	$(CC) -c $(CFLAGS) public07.c
public08.o: public08.c
	$(CC) -c $(CFLAGS) public08.c
public09.o: public09.c
	$(CC) -c $(CFLAGS) public09.c
public10.o: public10.c
	$(CC) -c $(CFLAGS) public10.c
machine.o: machine.c machine.h
	$(CC) -c $(CFLAGS) machine.c
interpreter.o: interpreter.c interpreter.h machine.h
	$(CC) -c $(CFLAGS) interpreter.c

clean:
	rm public*.x public*.o machine.o interpreter.o

