This program deals with SPIM instructions. The 
   instructions are in the form of:
   0x"Opcode""R1""R2""R3""Address"
   Opcode: 4 bytes, Ranges from 0-13 with each having a unique
   set of actions, and having different register inputs and
   memory requirements
   R1: 4 Bytes, Ranges from 0-11
   R2: 4 Bytes, Ranges from 0-11
   R3: 4 Bytes, Ranges from 0-11