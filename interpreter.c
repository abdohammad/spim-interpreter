#include <stdio.h>
#include "machine.h"
#include "interpreter.h"

int reg_jumped = 0;
int count1 = 0;
unsigned int separate_values[5] = {0};
Status value = 3;

void sep (Word instruction);

int load_program(Machine *const spim, const Word program[], int program_size){

  if(spim == NULL || program == NULL || program_size > MAX_MEMORY - 1 ){
    return 0;
  }else if( program_size <= 0 ){
    return 1;
  }

  spim->registers[11] = 0;
  for(count1 = 0; count1 < program_size; count1++){
    spim->memory[count1] = program[count1];
  }
  
  return 1;

}

Status run_SPIM_program(Machine *const spim, int max_instr,
                        int *const num_instr, int trace_flag){
  
  int op, reg_1, reg_2, reg_3, address;
  int trace = (spim->registers[11])/4;
  *num_instr = 0;

  if(spim == NULL || num_instr == NULL)
    return PARAMETER_ERROR;

    
  if(max_instr <= 0)
    return TIMEOUT;

  while(trace<MAX_MEMORY &&  trace<max_instr){
    
    sep(spim->memory[trace]);
    op = separate_values[0];
    reg_1 = separate_values[1];
    reg_2 = separate_values[2];
    reg_3 = separate_values[3];
    address = separate_values[4];
    
      
    if(valid_instruction(spim->memory[trace]) == 0 && op != 11)
      return INVALID_INSTRUCTION;
    

    if(op == 0)
      return HALTED;
    if(op==1)
      spim->registers[reg_1] = spim->registers[reg_2] + spim->registers[reg_3];
   
    if(op==2)
       spim->registers[reg_1] = spim->registers[reg_2] * spim->registers[reg_3];
    if(op==3)
      spim->registers[reg_1] = spim->registers[reg_2] * -1;
    if(op==4)
      spim->registers[reg_1] = spim->registers[reg_2] << address;
    if(op==5)
       spim->registers[reg_1] = spim->registers[reg_2] && spim->registers[reg_3];
    if(op==6)
      spim->registers[reg_1] = !spim->registers[reg_2];
    if(op==7)
      spim->registers[reg_1] = spim->memory[address/4];
    if(op==8)
      spim->registers[reg_1] = address;
    if(op==9)
      spim->memory[address/4] = spim->registers[reg_1];
    if(op==10)
       spim->registers[reg_1] = spim->registers[reg_2];
    if(op==11){
      if(spim->registers[reg_1] != spim->registers[reg_2] && address % 4 == 0 && address < MAX_MEMORY - 1){
	spim->registers[11] = address;
	*num_instr = *num_instr + 1;
      }else{
	 spim->registers[11] = spim->registers[11] + 4;
	 *num_instr = *num_instr + 1;
      }
	  }
    if(op==12)
      scanf("%d", &spim->registers[reg_1]);
    if(op==13)
      printf("%d\n",spim->registers[reg_1]);
    
 
    if(op!=11){
      spim->registers[11] = spim->registers[11] + 4;
      *num_instr = *num_instr + 1;
    }
    trace = spim->registers[11]/ 4;
    
  }
  return TIMEOUT;

}

int reset(Machine *const spim){

  if (spim == NULL){
    return 0;
  }else{
    spim->registers[11] = 0;
    return 1;
  }
}

void sep (Word instruction) {
 
  separate_values[0] = instruction &
    (0xF0000000);
  separate_values[0] = (separate_values[0] >> 28);
  
  separate_values[1] = instruction &
    (0x0F000000);
  separate_values[1] = (separate_values[1] >> 24);
  
  separate_values[2] = instruction &
    (0x00F00000);
  separate_values[2] = (separate_values[2] >> 20);

  separate_values[3] = instruction &
    (0x000F0000);
  separate_values[3] = (separate_values[3] >> 16);
 
  separate_values[4] = instruction &
    (0x0000FFFF);
}





